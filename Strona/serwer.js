var http = require('http');
var fs = require('fs');
var url = require('url');


http.createServer(function (req, res) {

  var q = url.parse(req.url, true);
  var filename = q.pathname;
  var nameIsEmpty = false;

  if (req.url === '/' || req.url === ''){
    filename = '/index.html'
    nameIsEmpty = true;
    }
    /*
  if (req.url === '/favicon.ico') {
    res.writeHead(204, {'Content-Type': 'image/x-icon'} );
    res.end();
    return;
    }*/

    if (req.url.indexOf('.ico') != -1) {
        fs.readFile('img' + filename, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        });

    }

  if(req.url.indexOf('index' ) == -1 && req.url.indexOf('firma') == -1 && req.url.indexOf('kontakt') == -1 && req.url.indexOf('oferta') == -1 && nameIsEmpty){ 
    fs.readFile('html' + filename, function (err, data) {
      if (err) console.log(err);
      res.writeHead(404, {'Content-Type': 'text/html'});
      res.write("404, Page not found");
      res.end();
    });
  }
 
  if(req.url.indexOf('.html') != -1){ 
  fs.readFile('html' + filename, function (err, data) {
    if (err) console.log(err);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

}

if(req.url.indexOf('.png') != -1){ 
  fs.readFile('img' + filename, function (err, data) {
    if (err) console.log(err);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

}

if(req.url.indexOf('.jpg') != -1){ 
  fs.readFile('img' + filename, function (err, data) {
    if (err) console.log(err);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

}

if(req.url.indexOf('.js') != -1){

  fs.readFile('js' + filename, function (err, data) {
    if (err) console.log(err);
    res.writeHead(200, {'Content-Type': 'text/javascript'});
    res.write(data);
    res.end();
  });

}

if(req.url.indexOf('.css') != -1){

  fs.readFile('css' + filename, function (err, data) {
    if (err) console.log(err);
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.write(data);
    res.end();
  });
}


}).listen(8080); 