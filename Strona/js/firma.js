var getParams = function (url) {
	var params;
	var query = url;
	var params = query.split('=');
	
	return params;
};




var coll = document.getElementsByClassName("c_banner");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.display = "block"
      content.style.maxHeight = null;
      content.style.verticalAlign = null;
      
    } else {
      content.style.display = "inline-block"
      content.style.maxHeight = "500px";
      content.style.verticalAlign = "middle";
    } 
  });
  coll[i].click();
}

setTimeout(WaitForCSS, 10);

function WaitForCSS(){
  var focus = getParams(window.location.href);
  if(focus[1] != null){
    document.getElementById(focus[1]).scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
  }
}
