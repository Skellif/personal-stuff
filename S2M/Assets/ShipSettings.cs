﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu()]
public class ShipSettings : ScriptableObject {
    //handling
    public float mass;
    public float thrustForward;
    public float thrustForwardEnergyCost;
    public float thrustBack;
    public float thrustSideways;
    public float thrustRotational;
    public float[] automaticStabilizers;

    //stats
    public float[] hp;
    public float[] armor;
    public float[] shields;
    public float maxEnergy;
    public float energyRegen;

    //weapons
    public GameObject[] mainArmaments;
    public GameObject[] secondaryArmaments;
    public GameObject[] torpedoTubes;
    public GameObject[] PDS;


}
