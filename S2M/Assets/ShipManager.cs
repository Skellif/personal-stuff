﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    public ShipSettings settings;
    [SerializeField]    private Rigidbody rigidbody;
    [SerializeField]    private bool cas;

    public float energy;


    [HideInInspector]    public bool settingsFoldout;

    private Vector3 rotation;
    private void OnEnable() {
        StartCoroutine("EnergyGeneration");
    }

    void Update()
    {
        
        rotation = this.transform.right * -1;
    }
    void FixedUpdate()
    {
        
        energy = Mathf.Max(energy - settings.thrustForwardEnergyCost * Time.deltaTime * (Mathf.Abs(Input.GetAxisRaw("Vertical")) + Mathf.Abs(Input.GetAxisRaw("Horizontal"))), 0);

        if(energy > settings.thrustForwardEnergyCost){
            rigidbody.AddForce(rotation * settings.thrustForward * Input.GetAxisRaw("Vertical") * Time.fixedDeltaTime);
            rigidbody.AddTorque(Vector3.up * settings.thrustRotational * Input.GetAxisRaw("Horizontal") * Time.fixedDeltaTime);    
        }
        
        if(cas){
            //rigidbody.drag = settings.automaticStabilizers[0];
            rigidbody.angularDrag = settings.automaticStabilizers[1];
        }
        else{
            rigidbody.drag = 0;
            rigidbody.angularDrag = 0;
        }
    }

    private IEnumerator EnergyGeneration(){
        while(true){
        energy = Mathf.Min(energy + settings.energyRegen, settings.maxEnergy);
        yield return new WaitForSeconds(1);
        }
        yield return null;

    }

    private bool Cas (){
        cas = !cas;
        if(cas){
            //rigidbody.drag = settings.automaticStabilizers[0];
            rigidbody.angularDrag = settings.automaticStabilizers[1];
        }
        else{
            //rigidbody.drag = 0;
            rigidbody.angularDrag = 0;
        }
        return true;
    }
}
