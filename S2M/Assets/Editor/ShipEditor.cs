﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ShipManager))]
public class ShipEditor : Editor {
    ShipManager shipManager;
    Editor shipEditor;



    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        DrawSettings(shipManager.settings, ref shipManager.settingsFoldout, ref shipEditor);
    }

    void DrawSettings(Object settings, ref bool foldout, ref Editor editor){
        if(settings != null){
            foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);
            if(foldout){
                CreateCachedEditor(settings, null, ref editor);
                editor.OnInspectorGUI();


            }
        }
    }


    private void OnEnable()
    {
        shipManager = (ShipManager)target;
    }
}
