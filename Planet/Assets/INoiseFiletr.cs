﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public interface INoiseFiletr {
    float Evaluate(Vector3 point);
}
