﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGenerator
{
    ShapeSettings settings;
    INoiseFiletr[] NoiseFilters;
    public MinMax elevationMinMax;
    public void UpdateSettings(ShapeSettings shapeSettings)
    {
        this.settings = shapeSettings;  
        NoiseFilters = new INoiseFiletr[settings.noiseLayers.Length];
        for (int i = 0; i < NoiseFilters.Length; i++)
        {
            NoiseFilters[i] = NoiseFilterFactory.CreateNoiseFilter(settings.noiseLayers[i].noiseSettings);
        }
        elevationMinMax = new MinMax();
    }
    public Vector3 CalculatePointOnPlanet(Vector3 PointOnUnitSphere){
        float elevation = 0;
        float firstLayerValue = 0;

        if(NoiseFilters.Length > 0){
            firstLayerValue = NoiseFilters[0].Evaluate(PointOnUnitSphere);
            if(settings.noiseLayers[0].enabled)
            {
                elevation += firstLayerValue;
            }
        }
        

        for (int i = 1; i < NoiseFilters.Length; i++)
        {
            if(settings.noiseLayers[i].enabled)
            {
                float mask = (settings.noiseLayers[i].useFirstLayerAsMask)? firstLayerValue : 1;
                elevation += NoiseFilters[i].Evaluate(PointOnUnitSphere) * mask;
            }
        }
        elevation = settings.planetRadious * (1 + elevation);
        elevationMinMax.AddValue(elevation);
        return PointOnUnitSphere * elevation;
    }
}
