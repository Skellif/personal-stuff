﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseFilterFactory
{
    public static INoiseFiletr CreateNoiseFilter(NoiseSettings settings)
    {
        switch(settings.filterType){
            case NoiseSettings.FilterType.Simple:
            return new SimpleNoiseFilter(settings.simpleNoiseSettings);
            case NoiseSettings.FilterType.Ridgid:
            return new RidgidNoiseFilter(settings.ridgidNoiseSetings);
        }
        return null;
    }
}
