﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoiseSettings
{   
    public enum FilterType {Simple, Ridgid};
    public FilterType filterType;


    //[ConditionalHide("filterType", 0)]
    
    public SimpleNoiseSettings simpleNoiseSettings;

    //[ConditionalHide("filterType", 1)]
    public RidgidNoiseSetings ridgidNoiseSetings;
    


    [System.Serializable]
    public class SimpleNoiseSettings{
        public float strength = 1;
        [Range(1,8)] public int numLayers = 1;
        public float roughness = 2;
        public float baseRoughness = 1;
        public float persistence = .5f;
        public float minValue;
        public Vector3 center;
    }


    [System.Serializable]
    public class RidgidNoiseSetings : SimpleNoiseSettings
    {
        public float weightMultiplier = .8f;

    }
}
